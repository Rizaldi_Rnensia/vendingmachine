﻿namespace VMLibrary.StaticEnum
{
    public enum DatabaseType
    {
        Sql,
        PgSql,
        MongoDb,
        TextFile
    }
}