﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using VMLibrary.DataAccess;
using VMLibrary.Models;

namespace VMLibrary.Repository.Sql
{
    public class RepoMakanan : SQLConnector, IMakanan
    {
        public List<ModelMakanan> GetMakanan_All()
        {
            List<ModelMakanan> output;
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                output = connection.Query<ModelMakanan>("dbo.spMakanan_GetAll").ToList();

            }
            return output;
        }
        public ModelMakanan GetMakanan_ById(int Id)
        {
            ModelMakanan output;
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@Id", Id);
                output = connection.Query<ModelMakanan>("dbo.spMakanan_GetById", p).FirstOrDefault();
            }
            return output;
        }

        public ModelMakanan UpdateMakanan(ModelMakanan model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@Id", model.Id);
                p.Add("@QtyStock", model.QtyStock);

                connection.Execute("dbo.spMakanan_Update", p, commandType: CommandType.StoredProcedure);
                model.Id = p.Get<int>("@Id");
                return model;
            }
        }
    }
}
