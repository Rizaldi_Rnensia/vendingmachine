﻿using System.Configuration;
using VMLibrary.DataAccess;
using VMLibrary.Repository.Sql;
using VMLibrary.StaticEnum;

namespace VMLibrary
{
    public static class GlobalConfig
    {
        public static IMakanan Makanan { get; set; }
        public static void InitializeConnections(DatabaseType db)
        {
            if (db.Equals(DatabaseType.Sql))
            {
                RepoMakanan repoMakanan = new RepoMakanan();

                Makanan = repoMakanan;
                
            }
            else if (db.Equals(DatabaseType.PgSql))
            {
                //PgSqlConnector pgSql = new PgSqlConnector();
                //Connection = pgSql;
            }
            else if (db.Equals(DatabaseType.MongoDb))
            {
                //MongoSqlConnector mongoSql = new MongoSqlConnector();
                //Connection = mongoSql;
            }
            else if (db.Equals(DatabaseType.TextFile))
            {
                //TextConnector text = new TextConnector();
                //Connection = text;
            }
        }
        public static string ConnString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }
    }
}
