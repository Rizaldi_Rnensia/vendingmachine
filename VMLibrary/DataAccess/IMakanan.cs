﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMLibrary.Models;

namespace VMLibrary.DataAccess
{
    public interface IMakanan
    {
        // Interface Inventory Barang
        //ModelMakanan CreateMakanan(ModelMakanan model);
        ModelMakanan UpdateMakanan(ModelMakanan model);
        List<ModelMakanan> GetMakanan_All();
        ModelMakanan GetMakanan_ById(int Id);
        //void DeleteMakanan(long Id);
    }
}