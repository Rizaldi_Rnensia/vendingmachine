﻿namespace VMLibrary.Models
{
    public class ModelMakanan
    {
        public int Id { get; set; }
        public string NamaMakanan { get; set; }
        public decimal Harga { get; set; }
        public double QtyStock { get; set; }
    }
}
