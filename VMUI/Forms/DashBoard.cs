﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using VMLibrary;
using VMLibrary.Models;
using VMUI.Forms;

namespace VMUI
{
    public partial class DashBoard : Form
    {
        private List<ModelMakanan> dataMakanan = new List<ModelMakanan>();
        CultureInfo culture = new CultureInfo("id-ID");
        public DashBoard()
        {
            InitializeComponent();
            LoadDataMakanan();
        }

        public void LoadDataMakanan()
        {
            dataMakanan = GlobalConfig.Makanan.GetMakanan_All();
            List<ModelMakanan> dataTemp = new List<ModelMakanan>();
            dataTemp = dataMakanan;
            PopulateDataMakanan(dataTemp);
            MakananGridView.Update();
            MakananGridView.Refresh();
        }

        void PopulateDataMakanan(IEnumerable<ModelMakanan> model)
        {
            MakananGridView.Rows.Clear();
            object qty;
            foreach (var item in model)
            {
                if (item.QtyStock <= 0)
                {
                    qty = "stok habis";
                }
                else
                {
                    qty = item.QtyStock;
                }
                MakananGridView.Rows.Add(new object[] {
                    item.NamaMakanan,
                    item.Harga.ToString("C", culture),
                    qty,
                    "Beli"
                });
                MakananGridView.Rows[MakananGridView.RowCount - 1].Tag = item;
            }
        }

        private void MakananGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)// beli
            {
                new Pembelian((ModelMakanan)MakananGridView.CurrentRow.Tag).ShowDialog();
                LoadDataMakanan();
            }
        }
    }
}
