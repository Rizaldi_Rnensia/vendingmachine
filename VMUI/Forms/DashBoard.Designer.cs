﻿namespace VMUI
{
    partial class DashBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.lblLogo = new System.Windows.Forms.Label();
            this.LayoutSubMenuInventory = new System.Windows.Forms.Panel();
            this.MakananGridView = new System.Windows.Forms.DataGridView();
            this.CdKodeBarang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CdSatuanBarang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CdStockInventory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CdBeli = new System.Windows.Forms.DataGridViewButtonColumn();
            this.panelHeader.SuspendLayout();
            this.LayoutSubMenuInventory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MakananGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(74)))), ((int)(((byte)(74)))));
            this.panelHeader.Controls.Add(this.lblLogo);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(787, 70);
            this.panelHeader.TabIndex = 1;
            // 
            // lblLogo
            // 
            this.lblLogo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblLogo.AutoSize = true;
            this.lblLogo.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogo.ForeColor = System.Drawing.Color.White;
            this.lblLogo.Location = new System.Drawing.Point(263, 9);
            this.lblLogo.Name = "lblLogo";
            this.lblLogo.Size = new System.Drawing.Size(279, 45);
            this.lblLogo.TabIndex = 7;
            this.lblLogo.Text = "Vending Machine";
            this.lblLogo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LayoutSubMenuInventory
            // 
            this.LayoutSubMenuInventory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.LayoutSubMenuInventory.Controls.Add(this.MakananGridView);
            this.LayoutSubMenuInventory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutSubMenuInventory.Location = new System.Drawing.Point(0, 70);
            this.LayoutSubMenuInventory.Margin = new System.Windows.Forms.Padding(0);
            this.LayoutSubMenuInventory.Name = "LayoutSubMenuInventory";
            this.LayoutSubMenuInventory.Size = new System.Drawing.Size(787, 357);
            this.LayoutSubMenuInventory.TabIndex = 8;
            // 
            // MakananGridView
            // 
            this.MakananGridView.AllowUserToAddRows = false;
            this.MakananGridView.AllowUserToDeleteRows = false;
            this.MakananGridView.AllowUserToResizeRows = false;
            this.MakananGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MakananGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.MakananGridView.BackgroundColor = System.Drawing.Color.White;
            this.MakananGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MakananGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(74)))), ((int)(((byte)(74)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(74)))), ((int)(((byte)(74)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MakananGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.MakananGridView.ColumnHeadersHeight = 36;
            this.MakananGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CdKodeBarang,
            this.CdSatuanBarang,
            this.CdStockInventory,
            this.CdBeli});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.ForestGreen;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.MakananGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.MakananGridView.EnableHeadersVisualStyles = false;
            this.MakananGridView.GridColor = System.Drawing.Color.WhiteSmoke;
            this.MakananGridView.Location = new System.Drawing.Point(14, 21);
            this.MakananGridView.Margin = new System.Windows.Forms.Padding(5);
            this.MakananGridView.Name = "MakananGridView";
            this.MakananGridView.ReadOnly = true;
            this.MakananGridView.RowHeadersVisible = false;
            this.MakananGridView.Size = new System.Drawing.Size(759, 322);
            this.MakananGridView.TabIndex = 94;
            this.MakananGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.MakananGridView_CellClick);
            // 
            // CdKodeBarang
            // 
            this.CdKodeBarang.FillWeight = 65.65144F;
            this.CdKodeBarang.HeaderText = "Nama Makanan";
            this.CdKodeBarang.Name = "CdKodeBarang";
            this.CdKodeBarang.ReadOnly = true;
            // 
            // CdSatuanBarang
            // 
            this.CdSatuanBarang.FillWeight = 65.65144F;
            this.CdSatuanBarang.HeaderText = "Harga";
            this.CdSatuanBarang.Name = "CdSatuanBarang";
            this.CdSatuanBarang.ReadOnly = true;
            // 
            // CdStockInventory
            // 
            this.CdStockInventory.FillWeight = 65.65144F;
            this.CdStockInventory.HeaderText = "Stok";
            this.CdStockInventory.Name = "CdStockInventory";
            this.CdStockInventory.ReadOnly = true;
            // 
            // CdBeli
            // 
            this.CdBeli.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.ForestGreen;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            this.CdBeli.DefaultCellStyle = dataGridViewCellStyle2;
            this.CdBeli.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CdBeli.HeaderText = "";
            this.CdBeli.MinimumWidth = 100;
            this.CdBeli.Name = "CdBeli";
            this.CdBeli.ReadOnly = true;
            this.CdBeli.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CdBeli.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // DashBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(787, 427);
            this.Controls.Add(this.LayoutSubMenuInventory);
            this.Controls.Add(this.panelHeader);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "DashBoard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VendingMachine";
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.LayoutSubMenuInventory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MakananGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Label lblLogo;
        private System.Windows.Forms.Panel LayoutSubMenuInventory;
        private System.Windows.Forms.DataGridView MakananGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn CdKodeBarang;
        private System.Windows.Forms.DataGridViewTextBoxColumn CdSatuanBarang;
        private System.Windows.Forms.DataGridViewTextBoxColumn CdStockInventory;
        private System.Windows.Forms.DataGridViewButtonColumn CdBeli;
    }
}

