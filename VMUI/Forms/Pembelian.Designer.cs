﻿namespace VMUI.Forms
{
    partial class Pembelian
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.lblPembelian = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAddPembelian = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUangKembalian = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUangPembayaran = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtHargaMakanan = new System.Windows.Forms.TextBox();
            this.PembelianGridView = new System.Windows.Forms.DataGridView();
            this.CdUangPecahan = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.CdLembaran = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelHeader.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PembelianGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(74)))), ((int)(((byte)(74)))));
            this.panelHeader.Controls.Add(this.lblPembelian);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(374, 70);
            this.panelHeader.TabIndex = 2;
            // 
            // lblPembelian
            // 
            this.lblPembelian.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPembelian.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPembelian.ForeColor = System.Drawing.Color.White;
            this.lblPembelian.Location = new System.Drawing.Point(3, 9);
            this.lblPembelian.Name = "lblPembelian";
            this.lblPembelian.Size = new System.Drawing.Size(368, 45);
            this.lblPembelian.TabIndex = 7;
            this.lblPembelian.Text = "Pembelian";
            this.lblPembelian.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.panel1.Controls.Add(this.btnAddPembelian);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtUangKembalian);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtUangPembayaran);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtHargaMakanan);
            this.panel1.Controls.Add(this.PembelianGridView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 70);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(374, 401);
            this.panel1.TabIndex = 103;
            // 
            // btnAddPembelian
            // 
            this.btnAddPembelian.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddPembelian.AutoSize = true;
            this.btnAddPembelian.BackColor = System.Drawing.Color.OrangeRed;
            this.btnAddPembelian.Enabled = false;
            this.btnAddPembelian.FlatAppearance.BorderSize = 0;
            this.btnAddPembelian.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddPembelian.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPembelian.ForeColor = System.Drawing.Color.White;
            this.btnAddPembelian.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddPembelian.Location = new System.Drawing.Point(16, 360);
            this.btnAddPembelian.Name = "btnAddPembelian";
            this.btnAddPembelian.Size = new System.Drawing.Size(345, 30);
            this.btnAddPembelian.TabIndex = 110;
            this.btnAddPembelian.Text = "Beli";
            this.btnAddPembelian.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddPembelian.UseVisualStyleBackColor = false;
            this.btnAddPembelian.Click += new System.EventHandler(this.btnAddPembelian_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 17);
            this.label2.TabIndex = 109;
            this.label2.Text = "Uang Kembalian";
            // 
            // txtUangKembalian
            // 
            this.txtUangKembalian.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUangKembalian.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUangKembalian.Enabled = false;
            this.txtUangKembalian.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUangKembalian.Location = new System.Drawing.Point(16, 139);
            this.txtUangKembalian.Name = "txtUangKembalian";
            this.txtUangKembalian.Size = new System.Drawing.Size(345, 25);
            this.txtUangKembalian.TabIndex = 108;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 17);
            this.label1.TabIndex = 107;
            this.label1.Text = "Uang Pembayaran";
            // 
            // txtUangPembayaran
            // 
            this.txtUangPembayaran.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUangPembayaran.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUangPembayaran.Enabled = false;
            this.txtUangPembayaran.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUangPembayaran.Location = new System.Drawing.Point(16, 87);
            this.txtUangPembayaran.Name = "txtUangPembayaran";
            this.txtUangPembayaran.Size = new System.Drawing.Size(345, 25);
            this.txtUangPembayaran.TabIndex = 106;
            this.txtUangPembayaran.TextChanged += new System.EventHandler(this.txtUangPembayaran_TextChanged);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(13, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 17);
            this.label12.TabIndex = 105;
            this.label12.Text = "Harga Makanan";
            // 
            // txtHargaMakanan
            // 
            this.txtHargaMakanan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHargaMakanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHargaMakanan.Enabled = false;
            this.txtHargaMakanan.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHargaMakanan.Location = new System.Drawing.Point(16, 35);
            this.txtHargaMakanan.Name = "txtHargaMakanan";
            this.txtHargaMakanan.Size = new System.Drawing.Size(345, 25);
            this.txtHargaMakanan.TabIndex = 104;
            // 
            // PembelianGridView
            // 
            this.PembelianGridView.AllowUserToResizeRows = false;
            this.PembelianGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PembelianGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PembelianGridView.BackgroundColor = System.Drawing.Color.White;
            this.PembelianGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PembelianGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(74)))), ((int)(((byte)(74)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(74)))), ((int)(((byte)(74)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PembelianGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.PembelianGridView.ColumnHeadersHeight = 36;
            this.PembelianGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CdUangPecahan,
            this.CdLembaran});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.ForestGreen;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PembelianGridView.DefaultCellStyle = dataGridViewCellStyle3;
            this.PembelianGridView.EnableHeadersVisualStyles = false;
            this.PembelianGridView.GridColor = System.Drawing.Color.WhiteSmoke;
            this.PembelianGridView.Location = new System.Drawing.Point(15, 178);
            this.PembelianGridView.Margin = new System.Windows.Forms.Padding(5);
            this.PembelianGridView.Name = "PembelianGridView";
            this.PembelianGridView.Size = new System.Drawing.Size(346, 166);
            this.PembelianGridView.TabIndex = 103;
            this.PembelianGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.PembelianGridView_CellValueChanged);
            this.PembelianGridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.PembelianGridView_RowsRemoved);
            // 
            // CdUangPecahan
            // 
            this.CdUangPecahan.FillWeight = 65.65144F;
            this.CdUangPecahan.HeaderText = "Uang Pecahan";
            this.CdUangPecahan.Items.AddRange(new object[] {
            "2000",
            "5000",
            "10000",
            "20000",
            "50000"});
            this.CdUangPecahan.Name = "CdUangPecahan";
            this.CdUangPecahan.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CdUangPecahan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // CdLembaran
            // 
            dataGridViewCellStyle2.NullValue = null;
            this.CdLembaran.DefaultCellStyle = dataGridViewCellStyle2;
            this.CdLembaran.FillWeight = 65.65144F;
            this.CdLembaran.HeaderText = "Lembaran";
            this.CdLembaran.Name = "CdLembaran";
            this.CdLembaran.ToolTipText = "input number only";
            // 
            // Pembelian
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.ClientSize = new System.Drawing.Size(374, 472);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelHeader);
            this.MaximumSize = new System.Drawing.Size(390, 511);
            this.MinimumSize = new System.Drawing.Size(390, 511);
            this.Name = "Pembelian";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pembelian";
            this.panelHeader.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PembelianGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Label lblPembelian;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAddPembelian;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUangKembalian;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUangPembayaran;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtHargaMakanan;
        private System.Windows.Forms.DataGridView PembelianGridView;
        private System.Windows.Forms.DataGridViewComboBoxColumn CdUangPecahan;
        private System.Windows.Forms.DataGridViewTextBoxColumn CdLembaran;
    }
}