﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using VMLibrary;
using VMLibrary.Models;

namespace VMUI.Forms
{
    public partial class Pembelian : Form
    {
        ModelMakanan _MakananModel = null;
        CultureInfo culture = new CultureInfo("id-ID");
        public Pembelian()
        {
            InitializeComponent();
        }

        public Pembelian(ModelMakanan model)
        {
            InitializeComponent();
            if (model.QtyStock > 0)
            {
                lblPembelian.Text = $"Pembelian {model.NamaMakanan}";
                panel1.Visible = true;
            }
            else
            {
                lblPembelian.Text = $"Stok {model.NamaMakanan} habis !";
                panel1.Visible = false;
            }
            txtHargaMakanan.Text = model.Harga.ToString("C", culture);
            btnAddPembelian.Enabled = false;
            _MakananModel = model;
        }

        private void PembelianGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 1)
            {
                if(e.RowIndex >= 0)
                {
                    var cdPecahan = (PembelianGridView.Rows[e.RowIndex].Cells["CdUangPecahan"] as DataGridViewComboBoxCell).FormattedValue.ToString();
                    var cdLembaran = (PembelianGridView.Rows[e.RowIndex].Cells["CdLembaran"]).FormattedValue.ToString();
                }
            }
            LoadTxtUangPembayaran();
        }

        private void LoadTxtUangPembayaran()
        {
            decimal sum = 0;
            for (int i = 0; i < PembelianGridView.Rows.Count; ++i)
            {
                if (PembelianGridView.Rows[i].Cells[1].Value !=null)
                {
                    sum += (Convert.ToDecimal(PembelianGridView.Rows[i].Cells[0].Value)) * (Convert.ToDecimal(PembelianGridView.Rows[i].Cells[1].Value));
                }
            }
            txtUangPembayaran.Text = sum.ToString("C", culture);
        }

        private void PembelianGridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            LoadTxtUangPembayaran();
        }

        private void txtUangPembayaran_TextChanged(object sender, EventArgs e)
        {
            if (!txtHargaMakanan.Text.Equals(string.Empty))
            {
                var harga = Convert.ToDecimal(txtHargaMakanan.Text.Replace("Rp", String.Empty).Replace(".", String.Empty));
                var bayar = Convert.ToDecimal(txtUangPembayaran.Text.Replace("Rp", String.Empty).Replace(".", String.Empty));
                if (bayar >= harga)
                {
                    btnAddPembelian.Enabled = true;
                    btnAddPembelian.BackColor = Color.ForestGreen;
                    txtUangKembalian.Text = (bayar - harga).ToString("C", culture);
                }
                else
                {
                    btnAddPembelian.Enabled = false;
                    btnAddPembelian.BackColor = Color.OrangeRed;
                    txtUangKembalian.Clear();
                }
            }
        }

        private void btnAddPembelian_Click(object sender, EventArgs e)
        {
            if (_MakananModel != null)
            {
                //var current = GlobalConfig.Makanan.GetMakanan_ById(_MakananModel.Id);
                _MakananModel.QtyStock -= 1;
                GlobalConfig.Makanan.UpdateMakanan(_MakananModel);
                if (!txtUangKembalian.Text.Equals(string.Empty) && !txtUangKembalian.Text.Equals("Rp0"))
                {
                    MessageBox.Show($"Terima kasih telah berbelanja, Kembalian anda sebesar {txtUangKembalian.Text}", "Information", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Terima kasih telah berbelanja", "Information", MessageBoxButtons.OK);
                }
                ClearTextForm();
                this.Close();
            }
        }

        private void ClearTextForm()
        {
            txtHargaMakanan.Clear();
            txtUangKembalian.Clear();
            txtUangPembayaran.Clear();
        }
    }
}
