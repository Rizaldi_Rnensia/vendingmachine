﻿using System;
using System.Windows.Forms;

namespace VMUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            VMLibrary.GlobalConfig.InitializeConnections(VMLibrary.StaticEnum.DatabaseType.Sql);
            Application.Run(new DashBoard());
        }
    }
}
